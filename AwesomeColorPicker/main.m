//
//  main.m
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
