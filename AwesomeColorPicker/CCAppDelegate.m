//
//  CCAppDelegate.m
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import "CCAppDelegate.h"
#import "CCExampleView.h"

@implementation CCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    
    // Set the root view controller
    UIViewController* mainViewController = [[UIViewController alloc] init];
    [self.window setRootViewController:mainViewController];
    
    CCExampleView* exampleView = [[CCExampleView alloc] init];
    [mainViewController setView:exampleView];
    
    return YES;
}

@end
