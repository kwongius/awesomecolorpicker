//
//  CCColorPickerProtocol.h
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CCColorPickerProtocol <NSObject>
@required

/**
 * Set the current color of the Color chooser
 */
- (void) setColor:(UIColor*)color;

/**
 * Get the current color from the Color chooser
 */
- (UIColor*) color;

/**
 * Set the previous color of the Color chooser
 */
- (void) setPreviousColor:(UIColor*)color;

/**
 * Get the previous color from the Color chooser
 */
- (UIColor*) previousColor;

@end
