#AwesomeColorPicker#

Here is an awesome color picker. The only problem is that it isn't implemented. How sad.

Please download it and modify to make it work correctly! Download the project:  
[ARC enabled](https://bitbucket.org/kwongius/awesomecolorpicker/get/master.zip)  
[No ARC](https://bitbucket.org/kwongius/awesomecolorpicker/get/non-ARC.zip)

Or you can turn in your code by [forking](https://bitbucket.org/kwongius/awesomecolorpicker/fork) it on BitBucket if you are feeling adventurous!

This assignment was created for [CS4962 - Mobile Application Programming](http://eng.utah.edu/~cs4962/)
